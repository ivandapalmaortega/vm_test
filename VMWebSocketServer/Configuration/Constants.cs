﻿using System.Configuration;

namespace VMWebSocketServer.Configuration
{
    public class Constants
    {
        public static readonly string MESSAGE_CONNECTION_OPENED = ConfigurationManager.AppSettings["messageUserConnected"].ToString();
        public static readonly string MESSAGE_CONNECTION_CLOSED = ConfigurationManager.AppSettings["messageUserDisconnected"].ToString();
        public static readonly string COMMAND_EXIT = ConfigurationManager.AppSettings["commandExit"].ToString();
        public static readonly string COMMAND_EXIT_ALL = ConfigurationManager.AppSettings["commandExitAll"].ToString();
        public static readonly string MESSAGE_EXIT_ALL = ConfigurationManager.AppSettings["messageExitAll"].ToString();        
    }
}
