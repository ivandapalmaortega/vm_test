﻿using Newtonsoft.Json;

namespace VMWebSocketServer.Model
{
    public class InputMessage
    {
        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("timestamp")]
        public string Timestamp { get; set; }

        public override string ToString()
        {
            return string.Format("{0}\t{1}: {2}", this.Timestamp, this.Username, this.Message);
        }

    }
}
