﻿using Fleck;
using System;
using VMWebSocketServer.Server;

namespace VMWebSocketServer
{
    class Program
    {

        static void Main()
        {   
            // Creating server and connection with host and port
            WebSocketServer socketServer = null;
            WebSocketServerConnection socketConnection = null;
            string host = "ws://127.0.0.1";
            string portConsole = null;
            int port = 0;
            bool successParsePortConsole = false;

            // Port will be only a number
            while (!successParsePortConsole)
            {
                Console.Clear();
                Console.Write("Insert port: ");
                portConsole = Console.ReadLine();
                successParsePortConsole = Int32.TryParse(portConsole, out port);                
            }

            // Fleck terminal will show message and only errors. Debug mode shows too much information
            FleckLog.Level = LogLevel.Error;

            // Create Fleck WebSocketServer 
            socketServer = new WebSocketServer(string.Format("{0}:{1}", host, port));

            // Create the connection with Fleck class
            socketConnection = new WebSocketServerConnection(socketServer);

            // Start server
            socketConnection.Start();

            // Server will remain listening Enter key is pushed
            Console.WriteLine(string.Format("Connected to {0}:{1}. Push Enter to close socket server", host, port));
            string str = Console.ReadLine();

            // Close all connections
            socketConnection.CloseAll();
        }

    }
}
