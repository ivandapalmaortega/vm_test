﻿using Fleck;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using VMWebSocketServer.Configuration;
using VMWebSocketServer.Model;

namespace VMWebSocketServer.Server
{
    public class WebSocketServerConnection
    {
        // Private attributes
        private readonly List<IWebSocketConnection> allSockets;
        private readonly WebSocketServer server;
        private Dictionary<string, string> dictionaryPortUsername;

        /// <summary>
        ///     Constructor
        /// </summary>
        /// <param name="socketServer"> Socket server dependency</param>
        public WebSocketServerConnection(WebSocketServer socketServer)
        {
            server = socketServer;
            allSockets = new List<IWebSocketConnection>();
            dictionaryPortUsername = new Dictionary<string, string>();
        }
        
        /// <summary>
        ///     Start the server
        /// </summary>
        public void Start()
        {
            server.Start(socket =>
            {
                socket.OnOpen = () => OnOpen(socket);
                socket.OnClose = () => OnClose(socket);
                socket.OnMessage = message => OnMessage(message, socket);
            });
        }
        
        /// <summary>
        ///     Open a new connection to the server
        /// </summary>
        /// <param name="socket"></param>
        private void OnOpen(IWebSocketConnection socket)
        {
            string username = socket.ConnectionInfo.Path.Replace("/",string.Empty);
            Console.WriteLine(string.Format(Constants.MESSAGE_CONNECTION_OPENED, DateTime.Now.ToLongTimeString(), username, socket.ConnectionInfo.Id.ToString()));
            allSockets.ToList().ForEach(s => s.Send(string.Format("{0} {1} just connected", DateTime.Now.ToLongTimeString(), username)));
            allSockets.Add(socket);
            dictionaryPortUsername.Add(socket.ConnectionInfo.Id.ToString(), username);
        }

        /// <summary>
        ///     Close one connection from the server
        /// </summary>
        /// <param name="socket">socket to be closed</param>
        private void OnClose(IWebSocketConnection socket)
        {
            string username = dictionaryPortUsername[socket.ConnectionInfo.Id.ToString()].ToString();
            Console.WriteLine(string.Format(Constants.MESSAGE_CONNECTION_CLOSED, DateTime.Now.ToLongTimeString(), username, socket.ConnectionInfo.Id.ToString()));
            allSockets.ToList().ForEach(s => s.Send(string.Format("{0} {1} just disconnected", DateTime.Now.ToLongTimeString(), username)));
            allSockets.Remove(socket);
        }

        /// <summary>
        ///     Send a message to the server and to all the other connections
        /// </summary>
        /// <param name="message">message sent</param>
        /// <param name="socket">socket with the properties</param>
        private void OnMessage(string message, IWebSocketConnection socket)
        {
            // InputMessage in JSON
            InputMessage inputMessage = JsonConvert.DeserializeObject<InputMessage>(message);
            string structuredMessage = inputMessage.ToString();
            string leftMessage = null;
            if (inputMessage.Message.ToLower().Equals(Constants.COMMAND_EXIT))
            {
                leftMessage = string.Format("{0}\t{1} just left the room", inputMessage.Timestamp, inputMessage.Username);
                Console.WriteLine(leftMessage);
                allSockets.ToList().ForEach(s => s.Send(leftMessage));
                Close(socket);
            }
            else if (inputMessage.Message.ToLower().Equals(Constants.COMMAND_EXIT_ALL))
            {
                leftMessage = Constants.MESSAGE_EXIT_ALL;
                Console.WriteLine(leftMessage);
                allSockets.ToList().ForEach(s => s.Send(leftMessage));
                CloseAll();
            }
            else
            {
                Console.WriteLine(structuredMessage);
                allSockets.ToList().ForEach(s => s.Send(structuredMessage));
            }
        }

        /// <summary>
        ///     Delete only the specify connection
        /// </summary>
        /// <param name="socketConnection">socketConnection to be deleted</param>
        public void Close(IWebSocketConnection socketConnection)
        {
            List<IWebSocketConnection> socketsToBeClosed = allSockets.Where(x => x.ConnectionInfo.Id == socketConnection.ConnectionInfo.Id).ToList();
            socketsToBeClosed.ForEach(s => s.Close());
        }

        /// <summary>
        /// Delete all connections
        /// </summary>
        public void CloseAll()
        {
            allSockets.ForEach(s => s.Close());
            server.ListenerSocket.Close();
        }
    }
}
