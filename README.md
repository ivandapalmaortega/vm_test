# Creación de un Socket usando la librería Fleck
<br/>
## Server
La creación de un servidor socket usando Fleck es bastante fácil. Basta con importar la librería Fleck usango NuGet y crear una instancia de la clase WebSocketServer pasando como parámetro al constructor la concatenacion del Host y el Puerto, separados por dos puntos (:).

Como podrás ver, la clase principal donde arranca el programa, Program.cs, lo primero que realizo es una petición de introducción del puerto. Si el puerto es un número, creará un socket con 127.0.0.1:PUERTO, para crear un server socket local.

Creo el WebServerSocket con el host y el puerto y lo paso directamente a mi clase WebSocketServerConnection, donde creo el socket y está disponible el método Start para arrancarlo.

Una vez arrancado se queda esperando a las siguientes acciones:

* Nuevas conexiones.
* Envío de mensajes. El server reenviará el mensaje a todos los usuarios conectados a ese socket.
* Pulsación de una tecla para finalizar el servidor o que un usuario teclee "exit" para su desconexión individual. También puede enviar "exit all" y los desconectará a todos.

También implementé un diccionario para tener identificado a los usuarios por el Id de su conexión al socket.

El servidor mostrará los mensajes de todos los usuarios, así como el timestamp de todas las acciones. Decidí poner el nivel de log a Error para que no mostrara demasiada información.

En cuanto a la **inyección de dependencia**, he usado una pequeña inyección por parámetro de constructor. El constructur de mi clase WebSocketServerConnection obtiene por parámetro un objecto de la clase Fleck.WebSocketServer y lo asigna directamente, con lo cual WebSocketServerConnection no tiene constancia de lo que contiene WebSocketServer. Podría haber usado librerías como Autofac o Costle Windsor, que es la que uso actualmente en mi día a día, pero no lo vi necesario aquí. Quizá si recibiera otro tipo de clases, generaría una interface y haría los objectos entrantes la implementaran.


<br/>
#### Estructura

Un proyecto no es solo la realización del mismo. Debe tener una estructura simple para que, si algún compañero continuara con él, pueda visualmente analizarlo todo de manera más sencilla. Yo siempre suelo estructurar el proyecto en carpetas con nombres apropiados.

En este caso tenemos las siguientes:

- **Configuration**: Contiene la clase Constants, que leerá del archivo App.config las keys añadidas en el apartado AppSettings. ¿Por qué hago esto? De esta manera, si queremos cambiar un valor, un mensaje, etc, no hace falta que generemos nuevos binarios, sino que todo está en la configuración.
- **Model**: Contiene el objeto de transporte usado para los mensajes entrantes. Esta clase define usuario y mensaje, y será deserializado a la entrada creando un nuevo objecto a partir de un JSON de entrada. Para ellos hago uso de la librería NewtonSoft Json Net, v8.0.3 (sin dependencias).
- **Server**: En ella se encuenrta la clase que maneja el servidor socket. Crea conexiones al server, las cierra y reenvia los mensajes entrantes a todos los conectados.



<br/><br/>
## Client
Podemos conectar varios clientes abriendo en varias pestañas el archivo socket.html. 

Este archivo html está compuesto por simples campos input y un botón el cual comprobará que la conexión está creada. De no ser así mostrará un mensaje "Unable to connect to Socket".

Introduciremos el usuario, el host y el puerto del socket creado y, si la conexión de establece, los input de conexión se bloquearán y el botón de conexión desaparecerá (un poco de css no viene de más). No se volverán a activar hasta que nos desconectemos pulsando el boton ``Leave Room`` o tecleando ``exit``.

Ahora podremos enviar mensajes en formato JSON, enviarndo el usuario y el mensaje y el servidor lo reenviará a todo aquel que esté conectado.

Todo esta conexión viene dada por métodos javascript que se conectan al socket y manejan la visibilidad y el set de css que se debe usar.

<br/>
## Mejoras
- Crear el cliente en C#. Ejemplo aquí http://putridparrot.com/blog/websockets-with-fleck/ y controlar que si no hay un socket con ese puerto ya creado llamar al método que crea el socket.
- Controlar usuario duplicado en el server.
- Borrar y ocultar el chat al dejar la sala (lo mantuve para que se viera el histórico de mensajes).
- Si quisiéramos seguir usando clientes en el navegador, usar funciones jQuery Ajax para llamar al controlador del server y abrir el puerto si no está abierto.
